package com.stolbunov.roman.homework_21.model.users.messages;

public class MessageFactory {
    public static Message createMessage() {
        return new Message("Hello World", MessageType.RECEIVE);
    }
}
