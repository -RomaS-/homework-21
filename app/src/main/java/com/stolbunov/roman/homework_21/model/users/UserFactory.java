package com.stolbunov.roman.homework_21.model.users;

import com.stolbunov.roman.homework_21.model.users.messages.MessageFactory;

import java.util.ArrayList;
import java.util.List;

public class UserFactory {
    private static List<String> usersImageURI = new ArrayList<>();
    private static List<String> listNames = new ArrayList<>();
    private static List<String> listSurnames = new ArrayList<>();

    static {
        additionRandomImageURI();
        additionRandomName();
        additionRandomSurname();
    }

    public static List<User> createUsers() {
        List<User> users = new ArrayList<>();
        for (int i = 0; i < usersImageURI.size(); i++) {
            User user = new User(i, usersImageURI.get(i), listNames.get(i), listSurnames.get(i));
            user.addNewMessage(MessageFactory.createMessage());
            users.add(user);
        }
        return users;
    }

    private static void additionRandomImageURI() {
        usersImageURI.add("https://s1.1zoom.ru/prev2/438/437636.jpg");
        usersImageURI.add("https://i2.rozetka.ua/goods/2130302/20695483_images_2130302239.jpg");
        usersImageURI.add("http://sherly.mobile9.com/download/media/549/wolverinem_sVrGwXdL.jpg");
        usersImageURI.add("https://www.decorel.by/wp-content/uploads/2015/03/thumb_05790.jpg");
        usersImageURI.add("https://i.ytimg.com/vi/96sH5avbMik/maxresdefault.jpg");
        usersImageURI.add("https://s00.yaplakal.com/pics/pics_original/8/6/5/4828568.jpg");
        usersImageURI.add("http://oboi-dlja-stola.ru/file/16648/760x0/16:9/%D0%93%D1%80%D1%8E-%D0%B8-%D0%BC%D0%B8%D0%BD%D1%8C%D0%BE%D0%BD%D1%8B.jpg");
        usersImageURI.add("http://pngimg.com/uploads/minions/minions_PNG57.png");
    }

    private static void additionRandomName() {
        listNames.add("Arthur");
        listNames.add("Leonard");
        listNames.add("Sheldon");
        listNames.add("Jack");
        listNames.add("Carl");
        listNames.add("Harry");
        listNames.add("Gregory");
        listNames.add("Richard");
    }

    private static void additionRandomSurname() {
        listSurnames.add("Smith");
        listSurnames.add("Miller");
        listSurnames.add("Reed");
        listSurnames.add("Cooper");
        listSurnames.add("Thomas");
        listSurnames.add("Gray");
        listSurnames.add("Lee");
        listSurnames.add("Morgan");
    }
}
