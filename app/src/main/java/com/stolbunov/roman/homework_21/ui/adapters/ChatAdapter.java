package com.stolbunov.roman.homework_21.ui.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.stolbunov.roman.homework_21.R;
import com.stolbunov.roman.homework_21.model.users.User;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ChatViewHolder> {
    private IChatItemClickListener listener;
    private List<User> users;

    public ChatAdapter(List<User> users) {
        this.users = users;
    }

    @NonNull
    @Override
    public ChatViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.item_chat, viewGroup, false);
        return new ChatViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ChatViewHolder chatViewHolder, int position) {
        chatViewHolder.bind(users.get(position));
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    private void onClick(User user) {
        if (listener != null) {
            listener.onItemClick(user);
        }
    }

    public void setListener(IChatItemClickListener listener) {
        this.listener = listener;
    }

    public interface IChatItemClickListener {
        void onItemClick(User user);
    }

    class ChatViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView imageUser;
        private TextView fullName;
        private TextView lastMessage;
        private TextView lastMessageTime;

        ChatViewHolder(@NonNull View itemView) {
            super(itemView);

            imageUser = itemView.findViewById(R.id.chat_image_user);
            fullName = itemView.findViewById(R.id.chat_full_name);
            lastMessage = itemView.findViewById(R.id.chat_last_message);
            lastMessageTime = itemView.findViewById(R.id.chat_time_message);
        }

        void bind(User user) {
            itemView.setOnClickListener((v -> onClick(user)));

            loadUserImage(user);
            fullName.setText(user.getFullName());
            lastMessage.setText(user.getLastMessage().getTextMessage());
            lastMessageTime.setText(user.getLastMessage().getMessageTime());
        }

        private void loadUserImage(User user) {
            Glide.with(itemView.getContext()).load(user.getImageURI()).into(imageUser);
        }
    }
}

