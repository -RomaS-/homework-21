package com.stolbunov.roman.homework_21.ui.screens;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.stolbunov.roman.homework_21.R;
import com.stolbunov.roman.homework_21.model.db.EmulatorDB;
import com.stolbunov.roman.homework_21.model.users.User;
import com.stolbunov.roman.homework_21.model.users.messages.Message;
import com.stolbunov.roman.homework_21.model.users.messages.MessageType;
import com.stolbunov.roman.homework_21.ui.adapters.MessageAdapter;

public class ListMessageActivity extends AppCompatActivity {
    public static final String KEY_USER_ID = "KEY_USER_ID";
    public static final int RC_USER_MESSAGE = 651;

    private boolean flagNewMessage = false;
    private int userID;

    private ImageButton sendMessage;
    private EditText newTextMessage;
    private TextView titleUserName;

    private MessageAdapter adapter;
    RecyclerView messageRV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_message);

        userID = getUserIDFromIntent();

        initView();
        checkOnSendMessage();

        User user = EmulatorDB.getInstance().getUserByID(userID);
        titleUserName.setText(user.getName());
        adapter = new MessageAdapter(user);

        createMessageRecyclerView(createLayoutManager());
    }

    private int getUserIDFromIntent() {
        Intent intent = getIntent();
        return intent.getIntExtra(KEY_USER_ID, 0);
    }

    private void checkOnSendMessage() {
        newTextMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = s.toString();
                if (!text.trim().isEmpty()) {
                    sendMessage.setEnabled(true);
                } else {
                    sendMessage.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private void initView() {
        titleUserName = findViewById(R.id.message_title_user_name);
        newTextMessage = findViewById(R.id.message_new_text);

        ImageButton back = findViewById(R.id.message_back);
        back.setOnClickListener(this::comeBack);

        ImageButton smile = findViewById(R.id.message_smiley);
        smile.setOnClickListener(this::showAllSmiley);

        sendMessage = findViewById(R.id.message_send);
        sendMessage.setOnClickListener(this::sendMessage);
        sendMessage.setEnabled(false);
    }

    @NonNull
    private RecyclerView.LayoutManager createLayoutManager() {
        return new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
    }

    private void createMessageRecyclerView(RecyclerView.LayoutManager manager) {
        messageRV = findViewById(R.id.message_rv);
        messageRV.setLayoutManager(manager);
        messageRV.setAdapter(adapter);
    }

    private void comeBack(@Nullable View view) {
        if (flagNewMessage) {
            Intent intent = new Intent();
            intent.putExtra(KEY_USER_ID, userID);
            setResult(RESULT_OK, intent);
        }
        finish();
    }

    private void sendMessage(View view) {
        String newMessage = newTextMessage.getText().toString();
        adapter.addNewMessage(new Message(newMessage));
        newTextMessage.setText("");
        messageRV.scrollToPosition(adapter.getItemCount() - 1);
        flagNewMessage = true;
    }

    private void showAllSmiley(View view) {

//        Temporary emulation of the incoming message
        String newMessageReceived = newTextMessage.getText().toString();
        adapter.addNewMessage(new Message(newMessageReceived, MessageType.RECEIVE));
        newTextMessage.setText("");
        flagNewMessage = true;
    }

    @Override
    public void onBackPressed() {
        comeBack(null);
    }
}
