package com.stolbunov.roman.homework_21.model.users;

import com.stolbunov.roman.homework_21.model.users.messages.Message;

import java.util.LinkedList;

public class User {
    private int id;
    private String imageURI;
    private String name;
    private String surname;
    private String fullName;

    private LinkedList<Message> listMessage;

    public User(String imageURI, String name, String surname) {
        this(0, imageURI, name, surname);
    }

    public User(int id, String imageURI, String name, String surname) {
        this.id = id;
        this.imageURI = imageURI;
        this.name = name;
        this.surname = surname;
        fullName = name + " " + surname;
        listMessage = new LinkedList<>();
    }

    public void addNewMessage(Message message) {
        listMessage.add(message);
    }

    public Message getLastMessage() {
        return listMessage.getLast();
    }

    public String getFullName() {
        return fullName;
    }

    public String getImageURI() {
        return imageURI;
    }

    public String getName() {
        return name;
    }

    public LinkedList<Message> getAllMessage() {
        return listMessage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
