package com.stolbunov.roman.homework_21.model.db;

import com.stolbunov.roman.homework_21.model.users.User;
import com.stolbunov.roman.homework_21.model.users.UserFactory;
import com.stolbunov.roman.homework_21.model.users.messages.Message;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class EmulatorDB {
    private static EmulatorDB db;
    private static List<User> users;
    private Map<Integer, LinkedList<Message>> messagesUser;

    private EmulatorDB() {
    }

    public static EmulatorDB getInstance() {
        if (db == null) {
            db = new EmulatorDB();
            users = UserFactory.createUsers();
        }
        return db;
    }

    public List<User> getUsers() {
        return users;
    }

    public User getUserByID(int userID) {
        return users.get(userID);
    }
}
