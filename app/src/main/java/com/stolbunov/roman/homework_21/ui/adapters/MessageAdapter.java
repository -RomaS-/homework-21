package com.stolbunov.roman.homework_21.ui.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.stolbunov.roman.homework_21.R;
import com.stolbunov.roman.homework_21.model.users.User;
import com.stolbunov.roman.homework_21.model.users.messages.Message;
import com.stolbunov.roman.homework_21.model.users.messages.MessageType;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MessageViewHolder> {
    private List<Message> messages;
    private User user;

    public MessageAdapter(User user) {
        this.messages = user.getAllMessage();
        this.user = user;
    }

    public void addNewMessage(Message message) {
        messages.add(message);
        notifyItemInserted(messages.size() - 1);
    }

    @NonNull
    @Override
    public MessageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view;
        MessageType messageType = MessageType.values()[viewType];
        if (messageType == MessageType.RECEIVE) {
            view = inflater.inflate(R.layout.item_message_receive, viewGroup, false);
        } else if (messageType == MessageType.SENT) {
            view = inflater.inflate(R.layout.item_message_sent, viewGroup, false);
        } else {
            throw new IllegalStateException("Unknown value of MessageType");
        }
        return new MessageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MessageViewHolder messageViewHolder, int position) {
        messageViewHolder.bind(messages.get(position));
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    @Override
    public int getItemViewType(int position) {
        Message message = messages.get(position);
        return message.getType().ordinal();
    }

    class MessageViewHolder extends RecyclerView.ViewHolder {
        TextView messageText;
        CircleImageView userImage;


        MessageViewHolder(@NonNull View itemView) {
            super(itemView);
            messageText = itemView.findViewById(R.id.message_text);
            userImage = itemView.findViewById(R.id.message_user_image);
        }

        void bind(Message message) {
            if (message.getType() == MessageType.RECEIVE) {
                loadUserImage();
            }
            messageText.setText(message.getTextMessage());
        }

        private void loadUserImage() {
            Glide.with(itemView.getContext()).load(user.getImageURI()).into(userImage);
        }
    }
}
