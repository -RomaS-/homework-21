package com.stolbunov.roman.homework_21.ui.screens;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.stolbunov.roman.homework_21.R;
import com.stolbunov.roman.homework_21.model.db.EmulatorDB;
import com.stolbunov.roman.homework_21.model.users.User;
import com.stolbunov.roman.homework_21.model.users.UserFactory;
import com.stolbunov.roman.homework_21.ui.adapters.ChatAdapter;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    private ChatAdapter chatAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        createChatAdapter();
        initChatRecyclerView(createLayoutManager());
    }

    @NonNull
    private RecyclerView.LayoutManager createLayoutManager() {
        return new LinearLayoutManager(
                this, LinearLayoutManager.VERTICAL, false);
    }

    private void createChatAdapter() {
        chatAdapter = new ChatAdapter(getUsersFromDB());
        chatAdapter.setListener(this::onItemClick);
    }

    private List<User> getUsersFromDB() {
        EmulatorDB db = EmulatorDB.getInstance();
        return db.getUsers();
    }

    private void initChatRecyclerView(RecyclerView.LayoutManager layoutManager) {
        RecyclerView chatRecyclerView = findViewById(R.id.rv_chat);
        chatRecyclerView.setLayoutManager(layoutManager);
        chatRecyclerView.setAdapter(chatAdapter);
    }

    private void onItemClick(User user) {
        Intent intent = new Intent(this, ListMessageActivity.class);
        intent.putExtra(ListMessageActivity.KEY_USER_ID, user.getId());
        startActivityForResult(intent, ListMessageActivity.RC_USER_MESSAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null && resultCode == RESULT_OK) {
            switch (requestCode) {
                case ListMessageActivity.RC_USER_MESSAGE:
                    int userID = data.getIntExtra(ListMessageActivity.KEY_USER_ID, 0);
                    chatAdapter.notifyItemChanged(userID);
            }
        }

    }
}
