package com.stolbunov.roman.homework_21.model.users.messages;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class Message {
    private String messageTime;
    private String textMessage;
    private MessageType type;

    private SimpleDateFormat format = new SimpleDateFormat("HH:mm", Locale.ENGLISH);

    public Message(String textMessage) {
        this(textMessage, MessageType.SENT);
    }

    public Message(String textMessage, MessageType type) {
        this.textMessage = textMessage;
        this.type = type;
        saveMessageTime();

    }

    private void saveMessageTime() {
        messageTime = format.format(System.currentTimeMillis());
    }

    public String getMessageTime() {
        return messageTime;
    }

    public String getTextMessage() {
        return textMessage;
    }

    public MessageType getType() {
        return type;
    }
}
